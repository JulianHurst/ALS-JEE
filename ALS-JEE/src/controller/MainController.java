package controller;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Set;
import java.util.logging.Logger;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import ASP.ASP;
import datamodel.Atom;
import datamodel.PointOfView;
import datamodel.Serie;
import datamodel.Weight;
import lemmatiseur.Lefff;
import synonymes.Synonymes;
import tokemisation.Tokemiseur;
import utils.Utils;


@RestController
public class MainController {
	@Autowired
	ServletContext ctx;
	
	private static final Logger LOGGER = Logger.getLogger(MainController.class.getName());

	@RequestMapping(value="/")
	public ModelAndView index(){
		return new ModelAndView("index");
	}
	
	@RequestMapping(value="/docs")
	public ModelAndView getDocs(){
		return new ModelAndView("docs");
	}
	
	@RequestMapping(value="/analyse",method=RequestMethod.GET)
	public ModelAndView getAnalyse(HttpServletRequest req){
		//ServletContext ctx=req.getServletContext();
		Lefff l = new Lefff("/res/fplm.fr.txt");
		ASP analyseur;
        Tokemiseur A = new Tokemiseur("/res/serieA.txt");
        Tokemiseur B = new Tokemiseur("/res/serieB.txt");
        Synonymes S=new Synonymes();
        Utils u = new Utils();
        S.setAdj(l.getAdj());
		//String path = ctx.getRealPath("//home/juju/ALSresources/txt/");
        String path = "/txt/";
		//System.out.println("Chemin fichiers "+path);
		try {
			req.setCharacterEncoding("UTF-8");
			path+=req.getParameter("texte");
			analyseur=new ASP(path,l.getAdj(),l.getNoms(),l.getNomsP());
			analyseur.analyse();
			String ntxt = l.traiteTexte(path,analyseur.getASP().find("adj").getArray());
			//System.out.println("\ntexte lemmatisé : "+ntxt+"\n");
			S.classifierTableau(ntxt.split(" "),A,B);
			return new ModelAndView("analyse","resultat",ntxt);
		} catch (NullPointerException e) {
			e.printStackTrace();
			return new ModelAndView("error","msg","Erreur : Le fichier demandé n'existe pas !");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return new ModelAndView("error","msg","Erreur : Problème d'encodage (UTF-8) !");
		}
	}

	@RequestMapping(value="/analysestring",method=RequestMethod.GET, produces = "text/plain;charset=UTF-8")
	public @ResponseBody
	String getAnalyseString(HttpServletRequest req){
        String path = "/txt/";
		try {
			req.setCharacterEncoding("UTF-8");
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		path+=req.getParameter("texte");
		return doAnalysis(path);
	}
	
	public String doAnalysis(String path) {
		String ntxt="";
		Lefff l = new Lefff("/res/fplm.fr.txt");
		ASP analyseur;
        Tokemiseur A = new Tokemiseur("/res/serieA.txt");
        Tokemiseur B = new Tokemiseur("/res/serieB.txt");
        Synonymes S=new Synonymes();
        Utils u = new Utils();
		String txt;
        S.setAdj(l.getAdj());
		//String path = ctx.getRealPath("//home/juju/ALSresources/txt/");
		//System.out.println("Chemin fichiers "+path);
		try {
			analyseur=new ASP(path,l.getAdj(),l.getNoms(),l.getNomsP());
			analyseur.analyse();
//			System.out.println(l.traiteTexteContenu(lectureTexte(ctx.getRealPath("/resources/pizza")),analyseur.getASP().find("adj").getArray()));
			ntxt = l.traiteTexte(path,analyseur.getASP().find("adj").getArray());
			//System.out.println("\ntexte lemmatisé : "+ntxt+"\n");
			S.classifierTableau(ntxt.split(" "),A,B);
			return ntxt;
		} catch (NullPointerException e) {
			e.printStackTrace();
			return ntxt;
		} 
//			catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			return ntxt;
//		}
	}
	
	private String getPath(HttpServletRequest req) {
		String path = null;
		try {
			req.setCharacterEncoding("UTF-8");
			path = "/txt/" + req.getParameter("texte");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return path;
	}
	
	@RequestMapping(value = "/atoms",method = RequestMethod.GET)
	public ArrayList<Integer> getAtoms(HttpServletRequest req) {
		String path = getPath(req);
		Utils u = new Utils();
		Lefff l = new Lefff("/res/fplm.fr.txt");
        Tokemiseur A = new Tokemiseur("/res/serieA.txt");
        Tokemiseur B = new Tokemiseur("/res/serieB.txt");
		String txt;
		try {
			txt = u.lectureTexte(path);
			txt = u.supprPonctuation(txt);
			txt = l.traiteAdj(txt);
			txt = txt.trim();
			//Gets atom indices from text contained in serieA or serieB
			ArrayList<Integer> atomIndices = u.getAtomIndices(A, B, txt);
			return atomIndices; 
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@RequestMapping(value="/lecture",method=RequestMethod.GET)
	public ModelAndView getTexte(HttpServletRequest req){
		//ServletContext ctx=req.getServletContext();
        Utils u = new Utils();
		//String path = ctx.getRealPath("//home/juju/ALSresources/txt/");
        String path = "/txt/";
		//System.out.println("Chemin fichiers "+path);
		try {
			req.setCharacterEncoding("UTF-8");
			path+=req.getParameter("texte");
			String ntxt = u.lectureTexte(path);
			return new ModelAndView("lecture","resultat",ntxt);
		} catch (IOException e) {
			e.printStackTrace();
			return new ModelAndView("error","msg","Erreur : Le fichier demandé n'existe pas !");
		}
	}
	
	@RequestMapping(value = "/mainInnocente/analyse",method = RequestMethod.GET)
	public ModelAndView getMainInnocenteAnalyse(HttpServletRequest req){
		try {
			req.setCharacterEncoding("UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String path = req.getParameter("texte");
		return new ModelAndView("maininnocenteanalyse", "path", path);
	}
	
	@RequestMapping(value = "/mainInnocente",method = RequestMethod.GET)
	public ModelAndView getMainInnocente(HttpServletRequest req){
		return new ModelAndView("maininnocente");
	}
	
	@RequestMapping(value = "/lectureTexte",method = RequestMethod.GET)
	public String getTextContent(HttpServletRequest req){
		try {
			req.setCharacterEncoding("UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String path = "/txt/" + req.getParameter("path");
		Utils u = new Utils();
		try {
			String txt = u.lectureTexte(path);
			return txt;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "Error : " + e.getLocalizedMessage();
		}
	}
	
	/**
	 * Retourne le contenu d'un fichier côté serveur
	 * @param file Le fichier à lire
	 * @return Le contenu du fichier
	 * @throws IOException
	 */
	public String lectureTexte(String file) throws IOException{
		StringBuffer content=new StringBuffer();
		String line;
		BufferedReader buf = new BufferedReader(new FileReader(file));
		while((line=buf.readLine())!=null){
			content.append(line);
		}
		buf.close();
		return content.toString();
	}
	
	public Serie getSerie(String atom) {
        Tokemiseur A = new Tokemiseur("/res/serieA.txt");
        Tokemiseur B = new Tokemiseur("/res/serieB.txt");
        Utils u = new Utils();
        
        return u.getSerie(A, B, atom);
	}
	
	private ArrayList<Atom> JsonToAtoms(String values){
		JsonReader reader = Json.createReader(new StringReader(values));
		JsonObject obj = reader.readObject();
		Set<String> j = obj.keySet();
		ArrayList<Atom> atoms = new ArrayList<>();
		Lefff l = new Lefff("/res/fplm.fr.txt");
		for(String i : j) {
			JsonObject item = obj.getJsonObject(i);
			String word = item.getString("word");
			String value = item.getString("value");
			Weight w;
			Serie s;

			if(value.equals("negative"))
				w = Weight.NEGATIVE;
			else if(value.equals("neutral"))
				w = Weight.NEUTRAL;
			else 
				w = Weight.POSITIVE;
		
			String at = l.traiteAdj(word);
			s = getSerie(at);
			
			Atom a = new Atom(at, w, s);
			atoms.add(a);
		}
		LOGGER.info("POV found");
		return atoms;
	}
	
	@RequestMapping(value = "/pointofview",method = RequestMethod.POST)
	public PointOfView getPointOfView(@RequestBody String values) {
		ArrayList<Atom> atoms = JsonToAtoms(values);
		Utils u = new Utils();
		return u.analyse(atoms);
	}
}
