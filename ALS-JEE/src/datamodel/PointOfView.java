package datamodel;

public enum PointOfView {
	INTROVERT,
	EXTROVERT,
	HESITANT
}
