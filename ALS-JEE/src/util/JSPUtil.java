package util;

import org.apache.commons.lang3.StringEscapeUtils;

public class JSPUtil {
	
	public static String escapeJSString(final String s){
		return StringEscapeUtils.escapeEcmaScript(s);
	}

}
