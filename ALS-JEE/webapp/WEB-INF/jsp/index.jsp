<title>ALS - Analyse des Logiques Subjectives</title>
</head>
<body>
<%@include file="navbar.jsp" %>
<div class="col-md-2"></div>
<div class="col-md-8">
	<h1>Analyse des Logiques Subjectives</h1>
	<p class="lead">L'Analyse des Logiques Subjectives (A.L.S.) est une m�thode originale d'analyse du discours qui permet de faire correspondre des modes caract�ristiques d'expression verbale avec des structures psychopathologiques.</p>
	<h4>Fran�ais</h4>
<p>L'A.L.S. (Analyse des Logiques Subjectives) est une m�thode d'analyse des mots (lex�mes) d'un texte parl� ou �crit, inspir�e par la psychanalyse, qui permet, sans recourir au non-verbal (intonations, gestes, mimiques), d'avoir une id�e de la personnalit� de l'auteur et de ceux qu'il peut esp�rer persuader ou s�duire.</p>
<br>
<h4>English</h4>
<p>A.L.S (Analysis of Subjective Logics) is an analytical method concerned with the words (lexical items) of a spoken or written text. Drawing on psychoanalysis, it allows one, without resorting to the non-verbal (intonations, gestures, mimics, etc.), to get an idea of the personality of the author as well as of those one expects to persuade or to entice.</p>
<br>
<h4>Deutsch</h4>
<p>Die A.L.S (Analyse der Subjektiven Logiken) ist eine Untersuchungsmethode der W�rter (lexikalische Einheiten) eines gesprochenen oder geschriebenen Textes, mit einer Inspiration der Psychoanalyse, der erlaubt, ohne sich an das Nichtverbale (Intonationen, Bewegungen, Mimiken, u.s.w.) zu wenden, eine Idee der Personalit�t des Autors und derjenigen zu bekommen, die er zu �berreden oder zu bezaubern hofft.</p>
<br>
<h4>Portugu�s</h4>
<p>A A.L.S. (An�lise das L�gicas Subjetivas) � um m�todo de an�lise das palavras (unidades lexicais) de um texto falado ou escrito, inspirado pela psican�lise, que permite, sem recorrer ao n�o-verbal (intona��es, gestos, m�micas, etc.), ter uma id�ia da personalidade do autor e daqueles que ele pode esperar persuadir ou seduzir.</p>
<br>
<h4>Espa�ol</h4>
<p>El A.L.S. (An�lisis de las L�gicas Subjectivas) es un m�todo de an�lisis de las palabras (lexemas) de un texto hablado o escrito, inspirado por la psicoan�lisis, que permite, sin recurrir al no verbal (intonaciones, gestos, m�micas), tener una idea de la personalidad del autor y de aquellos a los que puede esperar persuadir o seducir.</p>
<br>
<h4>Italiano</h4>
<p>L'A.L.S. (Analisi delle Logiche Soggettive, � un metodo di analisi delle parole ("lex�mes") di un testo parlato o scritto, ispirata per la psicanalisi, che permette, senza ricorrere al no-verbale (intonazioni, gesti, mimici), di avere un'idea della personalit� dell'autore e di quelli che pu� sperare di persuadere o sedurre.</p>
<br>
<hr class="myhline">
<p>Ce site a pour vocation de mettre � disposition une application informatique permettant l'�tude d'un texte par l'A.L.S.. Voici des exemples de textes sur lesquels appliquer ce programme :</p>  
	<ul>
	<li><a href="lecture?texte=Manifeste de Witold Gombrowicz">Manifeste de Witold Gombrowicz</a></li>
	<li><a href="lecture?texte=Pourquoi l'extr�me gauche fran�aise est la plus b�te du monde">Pourquoi l'extr�me gauche fran�aise est la plus b�te du monde</a></li>
	<li><a href="lecture?texte=D�masquer le r�el">D�masquer le r�el</a></li>
	<li><a href="lecture?texte=Les moutons �lectriques">Les moutons �lectriques</a></li>
	</ul>
</div>
<div class="col-md-2"></div>
</body>
</html>