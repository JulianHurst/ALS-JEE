<%@ taglib prefix="my" uri="MyUtilities" %>
<title>Lecture d'un texte</title>
<script type="text/javascript" src=<c:url value="/resources/js/analyser.js"/>></script>
</head>
<body onload="addlisten('${my:escapeJSString(param.texte)}')">
<%@include file="navbar.jsp" %>
<div class="col-md-2"></div>
<div class="col-md-8">
<h1>${param.texte}</h1>
<p class="lead">${resultat}</p>
<!-- <a class="button" href="analyse?texte=${param.texte}">Analyse</a>-->
<button id="analysebtn" class="btn btn-primary" >Analyser</button>
<div id="return"></div>
</div>
<div class="col-md-2"></div>
<footer class="sticky">
	<div class="col-md-12">
		<div id="alertfooter" class="alert alert-success alert-dismissable fade">
		</div>
	</div>
</footer>
</body>
</html>