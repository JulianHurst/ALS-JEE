<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Main Innocente</title>
</head>
<body>
<p>
La main innocente permet à des utilisateurs d'analyser des textes suivant la méthode de l'A.L.S. en déternminant eux-mêmes si les atomes sont utilisés <span style="color: blue">positivement</span> ou <span style="color: red">negativement</span>.
</p>
<ul>
	<li><a href="mainInnocente/analyse?texte=Manifeste de Witold Gombrowicz">Manifeste de Witold Gombrowicz</a></li>
	<li><a href="mainInnocente/analyse?texte=Pourquoi l'extrême gauche française est la plus bête du monde">Pourquoi l'extrême gauche française est la plus bête du monde</a></li>
	<li><a href="mainInnocente/analyse?texte=Démasquer le réel">Démasquer le réel</a></li>
	<li><a href="mainInnocente/analyse?texte=Les moutons électriques">Les moutons électriques</a></li>
</ul>
</body>
</html>