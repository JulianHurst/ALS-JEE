<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="my" uri="MyUtilities" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src=<c:url value="/resources/js/analyser.js"/>></script>
<title>Main Innocente</title>
</head>
<body onload="getMainInnocenteTextContent('${my:escapeJSString(path)}')">
<p id="text">Chargement...</p>
<button onclick="getAllValues()">Analyser</button>
<span id="result"></span>
</body>
</html>