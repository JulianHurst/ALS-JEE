var puncts = [",",".",";",":","!","?","(",")","…","\""];

function addlisten(texte){
    $('#analysebtn').on('click', function(event){
    	analyse(texte);
    });
}

function analysing(texte){
	var loading = $("#loading")[0];
	if(loading){
		$.ajax({
			type: "GET",
			url: "/ALS-JEE/analysestring?texte=" + texte,
			success: function(data){
				loading.style.display = "none";
				$("#text").html(data);
			}
		})
	}
}

function analyse(texte){
	if($("#return")[0].hasChildNodes())
		return;
	$("#return").html("<h1>Analyse en cours...</h1>")
	$.ajax({
		type: "GET",
		url: "analysestring?texte="+texte,
		success: function(data){
			$("#return").html("<h1>Analyse du texte \""+texte+"\"</h1>"+"<p class='lead'>"+data+"</p>");
			$("#alertfooter").html('\
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>\
				<strong>Success!</strong> Le texte a été analysé avec succès.\
			');
			$("#alertfooter").addClass("in");
		},
	});
}

function getMainInnocenteTextContent(path){
	var dat;
	getTextContent(path, function(data){
		getAtoms(path, function(analysis){
			mainInnocente(data, analysis);
		})
	})
}

function mainInnocente(content, analysis){
	var words = content.trim().split(" ");
	words = correctPunct(words);
	//var indices = getImportantWordIndices(words, analysis);
	createMainInnocente(words, analysis);
}

//function getImportantWordIndices(words, analysis){
//	var result = "";
//	var indices = [];
//	for(var i = 0 ; i < words.length ; i++){
//		//var word = delPunctuation(words[i]);
//		for(var j = 0 ; j < analysis.length ; j++)
//		if(i === analysis[j]){
//			indices.push(i); 
//		}
//	}
//	return indices;
//}

function correctPunct(words){
	var result = [];
	var punct = false;
	for(var i = 0 ; i < words.length ; i++){
		for(var j = 0; j < puncts.length ; j++){
			var ind = words[i].indexOf(puncts[j]);
			if(ind === 0){
				result.push(puncts[j]);				
				let next = words[i].replace(puncts[j], "");
				if(next !== "")
					result.push(words[i].replace(puncts[j], ""));
				punct = true;
			}
			else if(ind === (words[i].length - 1)){
				let next = words[i].replace(puncts[j], "");
				if(next !== "")
					result.push(words[i].replace(puncts[j], ""));
				result.push(puncts[j]);		
				punct = true;
			}
		}
		if(!punct)
			result.push(words[i]);
		else
			punct = false;
	}
	return result;
}

function delPunctuation(word){
	word = word.replace(/,/g,"");
	word=word.replace(/\./g,"");
	word=word.replace(/;/g,"");
	word=word.replace(/:/g,"");
	word=word.replace(/!/g,"");
	word=word.replace(/\?/g,"");
	word=word.replace(/\(/g,"");
	word=word.replace(/\)/g,"");

		//Peut être une liste de mots inutiles supplémentaire à enlever

	word=word.replace(/…/g,"");
	word=word.replace(/\"/g,"");
	word=word.replace(/ {2,}/g, " ");
	word=word.replace(/(\r\n|\n|\r)/g,"");
	return word;
}

function createMainInnocente(words, indices){
	var text = $("#text");
	text.html("");
	var nbPuncts = 0;
	let isPunct;
	for(var i = 0 ; i < words.length ; i++){
		if((isPunct = $.inArray(words[i], puncts)) !== -1){
			nbPuncts++;
		}
		if(isPunct === -1 && $.inArray(i - nbPuncts, indices) !== -1){
			var options = [];
			var values = {"negative":"red", "neutral":"black", "positive":"blue"};
			var node = $(document.createElement("select"));
			
			for(var j = 0 ; j < 3 ; j++){
				var option = $(document.createElement("option"))
						.val(words[i] + "-" + Object.keys(values)[j])
						.html(words[i])
						.css('color', Object.values(values)[j]);
				if(j === 1)
					option.prop('selected', true);
				options.push(option);
				node.append(option);
			}
			node.on('change', function(){
				var selected = this.value;
				var value = selected.substring(selected.indexOf("-") + 1, selected.length);
				this.style.color = values[value];
			})
		}
		else{
			var word;
			if(i !== words.length - 1)
				word = words[i] + " ";
			else
				word = words[i];
			node = $(document.createTextNode(word));
		}
		text.append(node);
	}
}

function getAllValues(){
	var selects = $("select");
	var result = {};
	for(var i = 0 ; i < selects.length ; i++){
		var option = selects[i].value;
		var word = option.substring(0, option.indexOf("-"));
		var value = option.substring(option.indexOf("-") + 1, option.length);
		result[i] = {
					"word":word,
					"value":value
				};
		//result[word] = value;
	}
	var jso = JSON.stringify(result);
	getPointOfView(JSON.stringify(result), function(data){
		$("#result").html(data);
	})
}

function getTextContent(path, callback){
	$.ajax({
		type: "GET",
		url: "/ALS-JEE/lectureTexte?path=" + path,
		dataType: "text",
		success: callback
	});
}

function getAnalysedText(path, callback){
	$.ajax({
		type: "GET",
		url: "/ALS-JEE/analysestring?texte=" + path,
		success: callback
	});
}

function getAtoms(path, callback){
	$.ajax({
		type: "GET",
		url: "/ALS-JEE/atoms?texte=" + path,
		success: callback
	})
}

function getPointOfView(values, callback){
	$.ajax({
		type: "POST",
		url: "/ALS-JEE/pointofview",
		data: values,
		success: callback,
		contentType: "application/json;charset=UTF8",
		dataType: 'json'
	})
}
