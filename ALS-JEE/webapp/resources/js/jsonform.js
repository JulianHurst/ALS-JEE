var err=0;

function getFormData($form){
    var unindexed_array = $form.serializeArray();
    var indexed_array = {};

    $.map(unindexed_array, function(n, i){
        indexed_array[n['name']] = n['value'];
    });

    return indexed_array;
}

function blur(){
	$('#jsonForm input').blur(function()
	{
		if($.trim($(this).val()).length == 0) {
			$(this).parents('div.form-group').addClass('has-error');
		}
	});
}

function jsonpost(){
	$('#jsonForm input').each(function(){
		if(($.trim($(this).val()).length == 0) && ($(this).attr("id")!="imgProfil")){
			$(this).parents('div.form-group').addClass('has-error');
			err++;
		}
		else
			$(this).parents('div.form-group').removeClass('has-error');
	})
	if(err>0){
		err=0;
		return;
	}
	var formData = getFormData($("#jsonForm"));
	var data = JSON.stringify(formData);
	$.ajax({
		type: "POST",
		url: "api/etudiants",
		data: data,
		success: function(data){$("#return").html("Profil crée avec succès !");},
	});
}